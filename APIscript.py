import requests
from openpyxl import Workbook

# Define the NetBox API URL for IP addresses
url = "https://netbox.wikimedia.org/api/ipam/ip-addresses/"

# Define your authentication token
token = 'Your Token Here'

# Define headers with authentication token
headers = {"Authorization": f"Token {token}"}

# Create a new Excel workbook and select the active sheet
wb = Workbook()
ws = wb.active

# Add headers to the spreadsheet
headers_list = [
    "ID",
    "IP Address",
    "VRF",
    "Status",
    "Role",
    "Tenant",
    "Assigned",
    "DNS Name",
]
ws.append(headers_list)

next_url = url

while next_url:
    # Send GET request to the NetBox API
    response = requests.get(next_url, headers=headers)

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse JSON response
        data = response.json()
        ip_addresses = data["results"]

        # Append IP address data to the spreadsheet
        for ip_address in ip_addresses:
            assigned_object = ip_address.get("assigned_object", "")
            if assigned_object:
                assigned_object_str = f"{assigned_object.get('name', '')} ({assigned_object.get('device', {}).get('display', '')})"
            else:
                assigned_object_str = ""
            row = [
                ip_address["id"],
                ip_address["address"],
                ip_address["vrf"]["name"] if ip_address["vrf"] else "",
                ip_address["status"]["label"] if ip_address["status"] else "",
                ip_address["role"]["label"] if ip_address["role"] else "",
                ip_address["tenant"]["name"] if ip_address["tenant"] else "",
                assigned_object_str,
                ip_address["dns_name"],
            ]
            ws.append(row)

        # Get the URL of the next page, if it exists
        next_url = data.get("next")
    else:
        print(f"Error fetching data: {response.status_code}")
        print(f"Response content: {response.content}")

# Save the workbook
wb.save("netbox_ip_addresses.xlsx")
print("Data appended successfully.")
